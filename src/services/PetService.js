import axios from 'axios';
class PetService{

    url = 'http://ec2-18-218-62-238.us-east-2.compute.amazonaws.com:8085/api/pet/'

    async findPetAll(){
        return axios.get(this.url + "findPetAll");
    }
    async createPet(pet){
        return axios.post(this.url + "create", pet);
    }

    async editPet(pet){
        return axios.put(this.url + "update", pet);
    }

    async deletePet(id){
        return axios.delete(this.url + "deleteById/" + id.toString());
    }
}
export default new PetService();
