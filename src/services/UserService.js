import axios from 'axios';

class UserService{

    url = 'http://ec2-18-218-62-238.us-east-2.compute.amazonaws.com:8085/api/user/'

    async findUserAll(){
        return axios.get(this.url + "findUserAll");
    }
    async createUser(user){
        return axios.post(this.url + "create", user);
    }

    async editUser(user){
        return axios.put(this.url + "update", user);
    }

    async deleteUser(id){
        return axios.delete(this.url + "deleteById/" + id.toString());
    }
}
export default new UserService();
