import Vue from 'vue'
import VueRouter from 'vue-router'
import UserLists from "@/views/UserLists";
import PetLists from "@/views/PetLists";
Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'UserLists',
        component: UserLists
    },
    {
        path: '/PetLists',
        name: 'PetLists',
        component: PetLists
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
